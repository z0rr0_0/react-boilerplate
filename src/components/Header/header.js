/**
 @description
 * Header component which will be used through out the application.
 */



import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import MenuIcon from "@material-ui/icons/Menu";
import Logout from "@material-ui/icons/PowerSettingsNew";
import Box from '@material-ui/core/Box';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Tooltip from '@material-ui/core/Tooltip';
const drawerWidth = 240;


const useStyles = makeStyles(theme => ({
  appBar: {
    marginLeft: drawerWidth,
    background:"#18202c",
    [theme.breakpoints.up("sm")]: {
      width: `calc(100% - ${drawerWidth}px)`
    }
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up("sm")]: {
      display: "none"
    }
  },
  title: {
    flexGrow: 1,
  },
}));

function ElevationScroll(props) {
  const { children, window } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
    target: window ? window() : undefined,
  });

  return React.cloneElement(children, {
    elevation: trigger ? 4 : 0,
  });
}

export default function HeaderComponent({ mobileOpen, setMobileOpen,props }) {
  const logout = () => {
    localStorage.clear();
    localStorage.setItem("LoggedOut", true);
    window.location.pathname = '/'
  }
  const classes = useStyles();
  
 

  function handleDrawerToggle() {
    setMobileOpen(!mobileOpen);
  }

  
  return (
    <Box display="block" className="displayNonePrint">
      <ElevationScroll {...props}>
    <AppBar position="fixed" className={classes.appBar} >
      <Toolbar>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          edge="start"
          onClick={handleDrawerToggle}
          className={classes.menuButton}
        >
          <MenuIcon />
        </IconButton>
        <Typography variant="h6" noWrap className={classes.title}>
          Welcome! {localStorage.getItem('name')}
        </Typography>
        <div>
          <Tooltip title="Log out">
              <IconButton
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                color="inherit"
                onClick={logout}
              >
                <Logout />
              </IconButton>
          </Tooltip>
            </div>
      </Toolbar>
    </AppBar>
    </ElevationScroll>
    </Box>
  );
}
