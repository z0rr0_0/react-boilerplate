/**
 @description
 * This file contains the left side bar i.e, "Drawer Component" and header i.e, "Header Component"
 */

import React, { useState } from "react";

import HeaderComponent from "../Header/header";
import DrawerComponent from "../Drawer/drawer";

const DrawerHeaderCombo = ({ componentNav, setComponentNav }) => {

  const [mobileOpen, setMobileOpen] = useState(false);

  return (
    <>
      <HeaderComponent {...{ mobileOpen, setMobileOpen }} />
      <DrawerComponent {...{ mobileOpen, setMobileOpen, componentNav, setComponentNav }} />
    </>
  );
}

export default DrawerHeaderCombo;
