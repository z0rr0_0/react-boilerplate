
/**
 @description
 * This is the entry point for the routes.
 * Lazy Loding is used for the routes to reduce the final build bundle size and to increase the initial loading spped fo the application.
 */

import React, { useState, Suspense, lazy } from "react";

import CssBaseline from "@material-ui/core/CssBaseline";
import { makeStyles } from "@material-ui/core/styles";
import { Route, Redirect, useLocation } from "react-router-dom";

import { PrivateRoute } from '../../components/PrivateRoute';
import DrawerNavCombo from './drawerNav';
import Loader from '../Loader/index';
import DrawerHeaderCombo from "./drawerAndHeader";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  main: {
    padding: theme.spacing(12, 0),
    width: "100%",
    marginLeft: "auto",
    marginRight: "auto"
  }
}));

// const RouteName = lazy(() => import("../../Route/Address"));


const DashboardMain = (props) => {
  const classes = useStyles();
  const [componentNav, setComponentNav] = useState();
  const location = useLocation()

  return (
    <div className={classes.root}>
      <CssBaseline />
      {/* if logged in check  */}
       <DrawerHeaderCombo />

      <Suspense fallback={<Loader open={true} loaderMessage="Loading..." />}>
      <main className={classes.main}>

        {/* <PrivateRoute exact path="/corporate/onboardFG" component={OnBoardCorporateFgNew} /> */}

        <Route exact path="/">
          <Redirect exact from="/" to={location} />
        </Route>

      </main>
      </Suspense>
    </div>
  );
};

export default DashboardMain;
