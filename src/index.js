import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux'
import reduxThunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension';
import Alert from 'react-s-alert';

import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

import Reducer from './reducers'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

const store = createStore(
    Reducer,
    composeWithDevTools(
        applyMiddleware(reduxThunk)
    )
);

const theme = createMuiTheme({
  palette: {
      primary: {
          main: '#384B97',
      },
      secondary: {
          main: '#EF8132',
      },
  },
  typography: {
      useNextVariants: true
  }
});

ReactDOM.render(
  <MuiThemeProvider theme={theme}>
        <Provider store={store}>
            <App />
            <Alert stack={{ limit: 3 }} />
        </Provider>
    </MuiThemeProvider>,
    document.getElementById('root')
);

reportWebVitals();
