
import React, {Component} from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { BrowserRouter, Route, Redirect } from "react-router-dom";
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import logo from './logo.svg';
import './App.css';
import DrawerHeaderCombo from './components/Dashboard/drawerAndHeader';
import MiniDrawer from './components/Drawer/miniDrawer';

const theme = createMuiTheme({
  overrides: {
    MuiStepIcon: {
      root: {
        '&$completed': {
          color: '#EF8132',
        },
        '&$active': {
          color: '#EF8132',
        },
      },
      active: {},
      completed: {},
    },
  },
  palette: {
    primary: {
      main: '#4A4E92',
    },
    secondary: {
      main: '#EF8132',
    },
  },

  typography: {
    useNextVariants: true,
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
  }

});

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoggedIn: false
    }
  }

  conponentDidMount = () => {}

  render() {

    return (
      <MuiThemeProvider theme={theme}>
        <BrowserRouter>
          < MiniDrawer />
        </BrowserRouter>
      </MuiThemeProvider>
    );
  }
}

function mapStateToProps(state) {
  return {

  };
}


function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    ...bindActionCreators({  }, dispatch)
  };
}

export default connect(
  null,
  null
)(App)
