
/**
 @description
 * This file contains generic functions. 
 * They are here in utility because we are using them across the application.
 */

import history from "./history";
import Alert from 'react-s-alert';
import { change } from "redux-form";

const hostname = window.location.hostname;

export let API_URL = hostname === 'localhost' ? 'https://gqtest.quikwallet.com' : `https://${hostname}`;
export const S3_BUCKET_URL = hostname === 'gqapi.quikwallet.com' ? 'https://s3-ap-south-1.amazonaws.com/gqprod/' : 'https://s3-ap-south-1.amazonaws.com/gqstaging/';
export const S3_BUCKET_TEMPLATE_URL = hostname === 'gqapi.quikwallet.com' ? 'https://gqprod.s3-ap-south-1.amazonaws.com/template/' : 'https://gqstaging.s3-ap-south-1.amazonaws.com/template/';
export const REACT_APP_S3_BUCKET_CONTENT = hostname === 'gqapi.quikwallet.com' ? 'gqprod' : 'gqstaging';

const REQUEST = "REQUEST";
const SUCCESS = "SUCCESS";
const FAILURE = "FAILURE";

export const TOKEN_EXPIRY_MESSAGE = "Session has expired. Login again!"

export const percentageRegExp = /^(?:\d*\.\d{1,2}|\d+)$/;
export const negativeNumberCheckRegex = /^\d+([.,]\d{1,2})?$/
export const positiveIntegerRegex = /^[1-9]\d*$/;
export const phoneRegExp = /^\d{10}$/;
export const bookletQuantityRegExp = /^[0-9]*$/;
export const panRegExp = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;
export const gstRegExp = /^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/;
export const boolRegExp = /^(0|1)$/;
export const pinRegExp = /^[1-9][0-9]{5}$/;
export const emailRegExp = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
export const csvRegex = /(\.csv)$/i;
export const nameRegExp = /[a-zA-Z]{3,30}/;
export const speacialRegExp = /[!@#$%^&*(),.?":{}|<>'-]/;
export const numberRegExp = /[0-9]/;
export const aplhabetCheck = /[a-zA-Z]/
export const panNoRegExp = /[0-9a-zA-Z]{10}/
export const passwordRegExp = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;
export const manualGVRegExp = /^\d{14}$/;
export const scannedGVRegExp = /^\d{22}$/;


/**
 * @param  {[object]} actionType [It would be the unique identifier for the actions and reducers]
 * @param  {[any]} data [It would be the payload which you need to pass]
 */
export function actionCreator(actionType, data) {
    return {
        type: actionType,
        payload: data
    };
}

/**
 * @param  {[string]} base [It will be unique identifier for every action types]
 * @return {[string]} [A string will be returned contcatened with base and type (type can be [REQUEST, SUCCESS, FAILURE)]
 */
export function createRequestActionTypes(base) {
    return [REQUEST, SUCCESS, FAILURE].reduce((requestTypes, type) => {
        requestTypes[type] = `${base}_${type}`;
        return requestTypes;
    }, {});
}

/**
 * [handleLoginRedirect - on login redirect to dashboard page and set the data in local storage of the browser for authentication] 
 * @param {[object]} response [Contains all the details of logged in user]
 * @param {[string]} targetUrl [After login where you want to send the user i.e, the route to be redirected after login]
 */
export function handleLoginRedirect(response, targetUrl) {
    localStorage.setItem("access_token", response.data.token);
    localStorage.setItem('UAR_ACS', JSON.stringify(response.data.modules));
    localStorage.setItem("isLoggedIn", true);
    localStorage.setItem("name", response.data.name)
    localStorage.setItem("role", response.data.role)
    history.push(targetUrl);
}


/**
 * [checkHttpStatus - When client hits the server i.e, API's]
 * @param  {[object]} response [it is the response coming from the API's which will get consumed by the client]
 * @return {} [Either you will get the JSON response or the Error]
 */
export function checkHttpStatus(response) {
    if (response.status >= 200 && response.status < 204) {
        return response.json();
    } else if (response.status === 204) {
        return true;
    } else if (response.status >= 400 && response.status < 500) {
        return response.json();
    } else {
        var error = new Error(response.statusText);
        error.response = response;
        throw error;
    }
}


/**
 * [jsonApiHeader - When client hits the server i.e, API's then we would be needing this func for asserting the header]
 * @param  {} accessToken [it is the key which needed to be passed for accessing the API's]
 * @return {[object]} [It will return an object containg Content-Type (which will be always "application/json") and Authorization (Key Value Pair)]
 */
export const jsonApiHeader = accessToken => {
    return {
        "Content-Type": "application/json",
        "Authorization": accessToken ? `Bearer ${accessToken}` : ""
    };
};


/**
 * [Generic method for success message]
 * @param  {[string]} message [You need to pass message to access this function]
 * @param  {[Integer]} timeout [This one is optional but if you will pass then you need to pass it in milliseconds (By default it would be 5000 milliseconds)]
 */
export const showSuccessMessage = (message, timeout) => {
    Alert.success(message, {
        position: 'bottom-right',
        effect: 'slide',
        timeout: timeout ? timeout : 5000,
    });
};

/**
 * [Generic method for Error message]
 * @param  {[string]} message [You need to pass message to access this function]
 * @param  {[integer]} timeout [This one is optional but if you will pass then you need to pass it in milliseconds (By default it would be 5000 milliseconds)]
 */
export const showErrorMessage = (message, timeout) => {
    Alert.error(message, {
        position: 'bottom-right',
        effect: 'slide',
        timeout: timeout ? timeout : 5000,
    });
};

/**
 * [Generic method for Warning message]
 * @param  {[string]} message [You need to pass message to access this function]
 * @param  {[integer]} timeout [This one is optional but if you will pass then you need to pass it in milliseconds (By default it would be 10000 milliseconds)]
 */
export const showWarningMessage = (message, timeout) => {
    Alert.warning(message, {
        position: 'bottom-right',
        effect: 'slide',
        timeout: timeout ? timeout : 5000,
    });
};

/**
 * @param  {[func]} dispatch  [dispatch is a function of the Redux store. You call store.dispatch to dispatch an action. This is the only way to trigger a state change.]
 * @param  {[string]} formReducerName [It would be the name of the redux-form from where this method will be called. It should be unique]
 * @param  {[array]} FormFieldNameArray [It would be array of strings. Strings would be the name of the Fields for which we want to change the value which will be unique ]
 */
export const ResetFormFields = (dispatch, formReducerName, FormFieldNameArray) => {
    FormFieldNameArray.map(value => {
        dispatch(change(formReducerName, value, null));
        return null;
    });
}

export const fetchTimeout = (url, ms, { signal, ...options } = {}) => {
    const controller = new AbortController()
    const promise = fetch(url, { signal: controller.signal, ...options })
    if (signal) signal.addEventListener("abort", () => controller.abort())
    const timeout = setTimeout(() => controller.abort(), ms)
    return promise.finally(() => clearTimeout(timeout))
}