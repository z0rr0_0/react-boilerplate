/**
 * @desc: Generic components for the inputs like textFields, Checkboxes, RadionButtons, Dropdowns.
 */
import React from "react";
import moment from 'moment';
import TextField from "@material-ui/core/TextField";
import Checkbox from "@material-ui/core/Checkbox";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import AsyncSelect from "react-select/async";
import Select from "react-select";
import { DatePicker } from "@material-ui/pickers";

import Aux from "./hoc/Auxiliary/Auxiliary";



export const renderPassword = ({
  input,
  required,
  id,
  name,
  label,
  type,
  variant,
  margin,
  inputProps,
  onChange,
  fullWidth,
  value,
  helperText,
  disabled,
  multiline,
  rows,
  onKeyPress,
  meta: { touched, error },
  ...custom
}) => {
  return (
    <Aux>
      <TextField
        required={required}
        fullWidth
        variant="outlined"
        margin="dense"
        rows={rows}
        id={id}
        name={name}
        label={label}
        type="password"
        multiline={multiline}
        disabled={disabled ? true : false}
        inputProps={inputProps ? inputProps : {}}
        onChange={event => onChange}
        onKeyPress={onKeyPress}
        value={value ? value : ""}
        error={touched && error ? true : false}
        helperText={touched && error}
        {...input}
        {...custom}
      />
    </Aux>
  );
};

export const renderTextField = ({
  input,
  required,
  id,
  name,
  label,
  type,
  variant,
  margin,
  inputProps,
  onChange,
  fullWidth,
  value,
  helperText,
  disabled,
  multiline,
  rows,
  onKeyPress,
  meta: { touched, error },
  ...custom
}) => {
  return (
    <Aux>
      <TextField
        type={type}
        required={required}
        fullWidth
        variant="outlined"
        margin="dense"
        rows={rows}
        id={id}
        name={name}
        label={label}
        multiline={multiline}
        className="inputTxtCap"
        disabled={disabled ? true : false}
        inputProps={inputProps ? inputProps : {}}
        onChange={event => onChange}
        onKeyPress={onKeyPress}
        value={value ? value : ""}
        error={touched && error ? true : false}
        helperText={touched && error}
        {...input}
        {...custom}
      />
    </Aux>
  );
};

export const renderFileUpload = ({
  input,
  required,
  id,
  name,
  label,
  onChange,
  onClick,
  key,
  inputProps,
  autoFocus,
  meta: { touched, error },
  disabled,
  ...custom
}) => {
  return (
    <TextField
      required={required}
      id={id}
      name={name}
      label={label}
      fullWidth
      autoFocus={autoFocus}
      variant="outlined"
      margin="dense"
      type="file"
      error={touched && error ? true : false}
      helperText={touched && error}
      onClick={onClick}
      disabled={disabled}
      onChange={e => input.onChange(e)}
      inputProps={inputProps}
      InputLabelProps={{
        shrink: true
      }}
    />
  );
};

export const renderSelectField = ({
  input,
  required,
  id,
  name,
  label,
  type,
  errorText,
  variant,
  margin,
  inputProps,
  onChange,
  fullWidth,
  value,
  helperText,
  customError,
  meta: { touched, error },
  ...custom
}) => {
  return (
    <Aux>
      <TextField
        select
        fullWidth
        variant="outlined"
        margin="dense"
        required={required}
        id={id}
        className="inputTxtCap selectinput"
        name={name}
        label={label}
        inputProps={inputProps ? inputProps : {}}
        onChange={event => onChange}
        value={value}
        error={touched && error ? true : false}
        helperText={touched && error}
        {...input}
        {...custom}
      />
    </Aux>
  );
};

export const renderRadioGroup = ({ input, value, ...rest }) => {
  return (
    <RadioGroup
      {...input}
      {...rest}
      aria-label="position"
      row
      value={value}
      onChange={(e, value) => input.onChange(value)}
    />
  );
};

export const renderCheckBox = ({
  input,
  label,
  checked,
  data,
  color,
  disabled,
  meta: { touched, error },
  custom
}) => {
  return (
    <Aux>
      <FormControlLabel
        control={
          <Checkbox
            {...input}
            {...custom}
            value={data}
            // color="#EF8132"
            color={color}
            disabled={disabled}
            checked={data === true ? true : false}
            onChange={(e, value) => input.onChange(value)}
          />
        }
        label={label}
      />
    </Aux>
  );
};

export const renderAutoCompleteSingleSelect = ({
  input,
  loadOptions,
  onInputChange,
  id,
  onBlur,
  value,
  defaultInputValue,
  name,
  placeholder,
  isDisabled,
}) => {
  return (
    <Aux>
      <AsyncSelect
        {...input}
        id={id}
        name={name}
        defaultOptions
        isClearable
        isDisabled={isDisabled}
        cache={false}
        backspaceRemoves={true}
        className="inputTxtCap autoSelect autoCompleteCust"
        placeholder={placeholder}
        value={value}
        defaultInputValue={defaultInputValue}
        loadOptions={loadOptions}
        onBlur={onBlur}
        onChange={onInputChange}
      />
    </Aux>
  );
};

export const renderSelect = ({
  input,
  id,
  name,
  options,
  placeholder,
  disabled,
  required
}) => {
  return (
      <Select
      {...input}
      id={id} 
      name={name} 
      options={options}
      value={(input.value === '') ? null : options.find(obj => obj.value === input.value)}
      required={required}
      className="inputTxtCap autoCompleteCust"
      onChange={(value) => input.onChange(value)}
      placeholder={placeholder}
      onBlur={(value) => input.onBlur()}
      isDisabled={disabled}
      />
  );
};

//Please do not use this for now
export const renderMonthYearPicker = ({ input, label, value, meta: { touched, error }, ...custom }) => {
  return (
      <DatePicker 
      {...input}
      {...custom} 
      onChange={val => input.onChange(val)}
      views={["year", "month"]}
      label="Card Expiry"
      minDate={moment()}
      inputVariant="outlined"
      disablePast={true}
      value={value}
      format="MM/YYYY"
    />
  );
};
