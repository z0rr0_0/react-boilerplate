
/**
 @description
 * This HOC should be used as a parent wrapper wherever you are returing two or more components
   inside the componnet itself.
 */

const auxiliary = (props) => props.children;

export default auxiliary;